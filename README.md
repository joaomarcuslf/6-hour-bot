# 6-hours-bot

Bot description

## Commands

### Starting Development

```
  $ yarn start:dev
```

This command will start your bot as an api, you can test your bot with this command.

### Starting Production

```
  $ yarn start
```

This command will start your bot as an api, but will run the compressed code.

### Checking your code

```
  $ yarn lint
```

This command will lint your code with the fix flag on.

### Compressing your code

```
  $ yarn compress
```

This command will compress your code.

### Deploy

```
  $ URL=https://bot-url.doman TOKEN=your-bot:token yarn deploy
```

This command will set a webhook for your bot, all you need will be an url where you have deployed this api and your bot token.
