const axios = require('axios');
const { TOKEN, URL } = process.env;

axios
  .post(`https://api.telegram.org/bot${TOKEN}/setWebhook?url=${URL}/message?token=${TOKEN}`)
  .then(response => console.log(response.data.description))
  .catch(error => console.error(error));

