const express = require('express');
const bodyParser = require('body-parser');
const axios = require('axios');
const moment = require('moment-timezone');
const PORT = process.env.PORT || 3000;
const hourRegex = /^([01]?\d|2[0-3]):([0-5]?\d)$/;

const app = express();

app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
);

app.get('/', (req, res) => res.end('ok'));

app.post('/message', function messagePostHandler(req, res) {
  const { message } = req.body;
  const { token } = req.query;

  if (!token || !message) {
    return res.end('no token or no message');
  }

  if (shouldAnswer(message)) {
    const text = createBotAnswer(message);

    axios
      .post(`https://api.telegram.org/bot${token}/sendMessage`, {
        chat_id: message.chat.id,
        reply_to_message_id: message.reply_to_message ? message.reply_to_message.message_id : message.message_id,
        text,
      })
      .then(() => res.end('ok'))
      .catch(err => res.end(`error :${err}`));
  } else {
    return res.end('ok');
  }
});

app.listen(PORT, function() {
  console.log(`Telegram app listening on port ${PORT}!`);
});

const createMessageArray = (message = '') => message.split(/[\s, -]/);

const shouldAnswer = (message = {}) => {
  const msgArray = createMessageArray(message.text);

  const hasBeenCalled = msgArray.some(word => word === '@six_hours_bot');

  return hasBeenCalled;
};

const createBotAnswer = (message = {}) => {
  const msgArray = createMessageArray(message.text);

  let givenHour = msgArray.find(msg => msg.match(hourRegex) !== null);

  givenHour = givenHour ? givenHour.split(':') : [];

  return getTextUntilHour(givenHour);
};

const getTextUntilHour = ([hour = '18', minutes = '00']) => {
  if (parseInt(hour) > 23 || parseInt(minutes) > 60) return 'A hora requisitada não existe'; //Não necessário pois a regex já trata isso

  const now = moment().tz('America/Sao_Paulo');
  const wantedHour = moment()
    .tz('America/Sao_Paulo')
    .hour(hour)
    .minutes(minutes);

  const minutesFromNow = wantedHour.diff(now, 'minutes');

  if (minutesFromNow < 0) return `Já Passou das ${hour}:${minutes}, pergunte-me novamente amanhã`;

  const minutesLeft = minutesFromNow % 60;
  const hoursLeft = Math.floor(minutesFromNow / 60);

  if (hoursLeft <= 0) {
    return `${minutesLeft > 1 ? 'Faltam' : 'Falta'} ${minutesLeft} ${
      minutesLeft > 1 ? 'minutos' : 'minuto'
    } para as ${hour}:${minutes}`;
  } else {
    return `${hoursLeft > 1 ? 'Faltam' : 'Falta'} ${hoursLeft} ${hoursLeft > 1 ? 'horas' : 'hora'}${
      minutesLeft > 0 ? ` e ${minutesLeft} ${minutesLeft > 1 ? 'minutos' : 'minuto'}` : ''
    } para as ${hour}:${minutes}`;
  }
};
